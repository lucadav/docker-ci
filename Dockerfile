# this same shows how we can extend/change an existing official image from Docker Hub

FROM riot/riotbuild
# highly recommend you always pin versions for anything beyond dev/learn

# clone the repository to install RIOT
#RUN git clone --recursive https://github.com/RIOT-OS/Tutorials


WORKDIR /docker-ci

COPY . .

WORKDIR /docker-ci/task-01

CMD [ "make","all"]

# change working directory to root of nginx webhost
# using WORKDIR is preferred to using 'RUN cd /some/path
